.ONESHELL:

build-tau:
	@ echo "Build tau..."
	@ mkdir -p build
	@ CGO_ENABLED=0 go build -o build/tau *.go
	@ echo "Build complete"

run-tau:
	@ ./build/tau --dir="./temp/node"

rebuild-tau: build-tau run-tau