package core

import (
	"bytes"
	"fmt"
	"log"
	"net"
	"path"
	"time"

	bolt "github.com/boltdb/bolt"

	helpers "tau/helpers"
)

// Network - tool for discover network
type Network struct {
	tau *Tau
	db  *bolt.DB
}

// GetGateways - get Tau network gateways from db
func (n *Network) GetGateways() ([]string, error) {
	var (
		result = []string{}
	)

	err := n.db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte("gateways"))
		cursor := bucket.Cursor()
		prefix := []byte("gateway:")

		for k, v := cursor.Seek(prefix); k != nil && bytes.HasPrefix(k, prefix); k, v = cursor.Next() {
			result = append(result, string(v))
		}

		return nil
	})

	return result, err
}

// AddGateway - add new gateway
func (n *Network) AddGateway(address string) error {
	n.DiscoverNode(address)

	return n.db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte("gateways"))
		return bucket.Put([]byte(fmt.Sprintf("gateway:%s", address)), []byte(address))
	})
}

// CreateNetwork - create new network tool instance
func (t *Tau) CreateNetwork() error {
	networkDBFile := path.Join(t.nodeDir, "discover")
	db, err := bolt.Open(networkDBFile, 0600, &bolt.Options{Timeout: 1 * time.Second})
	if err != nil {
		return err
	}

	err = db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("gateways"))
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}

	t.Network = &Network{
		tau: t,
		db:  db,
	}

	return nil
}

// DiscoverNode - discover node
func (n *Network) DiscoverNode(address string) error {
	hello, err := n.CreateHelloRequest()
	if err != nil {
		log.Println("Create HELLO request:", err.Error())
		return err
	}

	compressedHello, err := helpers.CompressObject(hello)
	if err != nil {
		log.Println("Compress HELLO request:", err.Error())
		return err
	}

	conn, err := net.Dial("udp", address)
	if err != nil {
		log.Println("Connect to node:", err.Error())
		return err
	}

	_, err = conn.Write(compressedHello)
	if err != nil {
		log.Println("Write to connection:", err.Error())
		return err
	}
	defer conn.Close()

	return nil
}
