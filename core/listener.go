package core

import (
	"fmt"
	"log"
	"net"

	helpers "tau/helpers"
)

// Handle - handle input connection
func (t *Tau) Handle(connection *net.UDPConn) {
	buffer := make([]byte, 4096)
	count, _, err := connection.ReadFromUDP(buffer)
	if err != nil {
		log.Println("Handle connection:", err.Error())
		return
	}

	request := &HelloRequest{}
	err = helpers.DecompressObject(buffer[:count], request)
	if err == nil {
		// Processing HELLO request
		if request.Gateway {
			
		}
		return
	}
}

// Run - run listener
func (t *Tau) Run() error {
	if t.asGateway {
		udpAddress, err := net.ResolveUDPAddr("udp4", fmt.Sprintf("0.0.0.0:%s", t.port))
		log.Println("Run as gateway:", udpAddress.String())

		connection, err := net.ListenUDP("udp", udpAddress)
		if err != nil {
			return err
		}
		defer connection.Close()

		for {
			t.Handle(connection)
		}
	} else {
		log.Println("Run as client...")
	}

	return nil
}
