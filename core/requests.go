package core

// HelloRequest - network access request
type HelloRequest struct {
	ID      string   `json:"id"`
	Key     string   `json:"key"`
	DataKey string   `json:"data_key"`
	Gateway bool     `json:"gateway"`
	Aliases []string `json:"aliases"`
}

// HelloResponse - network access response
type HelloResponse struct {
}

// CreateHelloRequest - create network access request
func (n *Network) CreateHelloRequest() (*HelloRequest, error) {
	b64pem, err := n.tau.ECDSA.B64PEMPublicKey()
	if err != nil {
		return nil, err
	}
	b64RSA, err := n.tau.RSA.B64PEMPublicKey()
	if err != nil {
		return nil, err
	}
	return &HelloRequest{
		ID:      n.tau.ID,
		Key:     b64pem,
		DataKey: b64RSA,
		Gateway: n.tau.asGateway,
		Aliases: n.tau.AliasIDs,
	}, nil
}
