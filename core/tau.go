package core

import (
	"crypto/sha512"
	"fmt"
	"log"
	"strings"

	crypto "tau/crypto"
)

// Tau - core system object
type Tau struct {
	nodeDir   string
	port      string
	asGateway bool
	ECDSA     *crypto.ECDSA
	RSA       *crypto.RSA
	Network   *Network
	ID        string
	AliasIDs  []string
}

// Init - initialization of node
func Init(dir, aliases, port, firstGateway string, asGateway bool) (*Tau, error) {
	ecdsa, err := crypto.InitECDSA(dir)
	if err != nil {
		return nil, err
	}
	privateKey, err := ecdsa.PEMPrivateKey()
	if err != nil {
		return nil, err
	}

	rsa, err := crypto.InitRSA(dir)
	if err != nil {
		return nil, err
	}

	tau := &Tau{
		port:      port,
		nodeDir:   dir,
		asGateway: asGateway,
		ECDSA:     ecdsa,
		RSA:       rsa,
		ID:        fmt.Sprintf("%x", sha512.Sum512(privateKey)),
		AliasIDs:  strings.Split(aliases, ";"),
	}

	tau.CreateNetwork()

	if len(firstGateway) > 0 {
		err := tau.Network.AddGateway(firstGateway)
		if err != nil {
			log.Println(err.Error())
		}
	}

	gateways, err := tau.Network.GetGateways()
	if err != nil {
		return nil, err
	}

	if asGateway {
		log.Println("Run as gateway for Tau network")
	}

	if len(gateways) < 1 {
		log.Println("No gateways for discovering. Run tau with --gate=address paramether to get the first gateways")
	}

	return tau, nil
}
