package crypto

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"io/ioutil"
	"os"
	"path"
)

// ECDSA - main ECDSA cryptoprovider
type ECDSA struct {
	private *ecdsa.PrivateKey
}

// PEMPrivateKey - dump of private key in PEM format
func (e *ECDSA) PEMPrivateKey() ([]byte, error) {
	x509Enc, err := x509.MarshalECPrivateKey(e.private)
	if err != nil {
		return []byte{}, err
	}

	return pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Enc}), nil
}

// PEMPublicKey - dump of public key in PEM format
func (e *ECDSA) PEMPublicKey() ([]byte, error) {
	x509Enc, err := x509.MarshalPKIXPublicKey(e.private.Public())
	if err != nil {
		return []byte{}, err
	}

	return pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509Enc}), nil
}

// B64PEMPublicKey - dump of public key in base64-encoded PEM format
func (e *ECDSA) B64PEMPublicKey() (string, error) {
	pem, err := e.PEMPublicKey()
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(pem), nil
}

// DumpKeys - dump key pair to files
func (e *ECDSA) DumpKeys(dir string) error {
	privateKeyFile := path.Join(dir, "key")

	dumpPrivateKey, err := e.PEMPrivateKey()
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(privateKeyFile, dumpPrivateKey, 0600)
	if err != nil {
		return err
	}

	return nil
}

// CreateECDSA - generate ECDSA key pair
func CreateECDSA() (*ECDSA, error) {
	privateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		return nil, err
	}
	return &ECDSA{private: privateKey}, nil
}

// LoadECDSA - load csp from files
func LoadECDSA(dir string) (*ECDSA, error) {
	privateKeyFile := path.Join(dir, "key")

	privateKeyData, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(privateKeyData)
	x509Enc := block.Bytes
	privateKey, err := x509.ParseECPrivateKey(x509Enc)
	if err != nil {
		return nil, err
	}

	return &ECDSA{private: privateKey}, nil
}

// InitECDSA - initialize ECDSA CSP
func InitECDSA(dir string) (*ECDSA, error) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		os.MkdirAll(dir, 0700)
	}

	if _, err := os.Stat(path.Join(dir, "key")); os.IsNotExist(err) {
		ecdsa, err := CreateECDSA()
		if err != nil {
			return nil, err
		}

		err = ecdsa.DumpKeys(dir)
		if err != nil {
			return nil, err
		}

		return ecdsa, nil
	}

	return LoadECDSA(dir)
}
