package crypto

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"io/ioutil"
	"os"
	"path"
)

// RSA - main RSA cryptoprovider
type RSA struct {
	private *rsa.PrivateKey
}

// PEMPrivateKey - dump of private key in PEM format
func (r *RSA) PEMPrivateKey() ([]byte, error) {
	x509Enc := x509.MarshalPKCS1PrivateKey(r.private)
	return pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Enc}), nil
}

// PEMPublicKey - dump of public key in PEM format
func (r *RSA) PEMPublicKey() ([]byte, error) {
	x509Enc, err := x509.MarshalPKIXPublicKey(r.private.Public())
	if err != nil {
		return []byte{}, err
	}

	return pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509Enc}), nil
}

// B64PEMPublicKey - dump of public key in base64-encoded PEM format
func (r *RSA) B64PEMPublicKey() (string, error) {
	pem, err := r.PEMPublicKey()
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(pem), nil
}

// DumpKeys - dump key pair to files
func (r *RSA) DumpKeys(dir string) error {
	privateKeyFile := path.Join(dir, "rsa-key")

	dumpPrivateKey, err := r.PEMPrivateKey()
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(privateKeyFile, dumpPrivateKey, 0600)
	if err != nil {
		return err
	}

	return nil
}

// CreateRSA - generate RSA key pair
func CreateRSA() (*RSA, error) {
	privateKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, err
	}
	return &RSA{private: privateKey}, nil
}

// LoadRSA - load csp from files
func LoadRSA(dir string) (*RSA, error) {
	privateKeyFile := path.Join(dir, "rsa-key")

	privateKeyData, err := ioutil.ReadFile(privateKeyFile)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(privateKeyData)
	x509Enc := block.Bytes
	privateKey, err := x509.ParsePKCS1PrivateKey(x509Enc)
	if err != nil {
		return nil, err
	}

	return &RSA{private: privateKey}, nil
}

// InitRSA - initialize RSA CSP
func InitRSA(dir string) (*RSA, error) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		os.MkdirAll(dir, 0700)
	}

	if _, err := os.Stat(path.Join(dir, "rsa-key")); os.IsNotExist(err) {
		rsaKey, err := CreateRSA()
		if err != nil {
			return nil, err
		}

		err = rsaKey.DumpKeys(dir)
		if err != nil {
			return nil, err
		}

		return rsaKey, nil
	}

	return LoadRSA(dir)
}
