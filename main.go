package main

import (
	"flag"
	"log"

	core "tau/core"
)

var (
	nodeDir        string
	addressAliases string
	port           string
	gateway        string
	asGateway      bool

	tau *core.Tau
)

func init() {
	var (
		err error
	)

	// Application flags
	flag.StringVar(&nodeDir, "dir", "./node", "path to node dir")
	flag.BoolVar(&asGateway, "asgw", false, "run node for discovering another nodes")
	flag.StringVar(&addressAliases, "aliases", "", "aliases of node id")
	flag.StringVar(&port, "port", "9037", "port for udp connections")
	flag.StringVar(&gateway, "gate", "", "first gateway")
	flag.Parse()

	// Node initialization
	tau, err = core.Init(nodeDir, addressAliases, port, gateway, asGateway)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func main() {
	err := tau.Run()
	if err != nil {
		log.Fatalln(err.Error())
	}
}
