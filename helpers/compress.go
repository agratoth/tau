package helpers

import (
	"bytes"
	"compress/gzip"
	"encoding/json"
	"io"
)

// CompressObject - create gzip-compressed packet
func CompressObject(obj interface{}) ([]byte, error) {
	var (
		buffer bytes.Buffer
	)

	data, err := json.Marshal(obj)
	if err != nil {
		return []byte{}, err
	}

	gz := gzip.NewWriter(&buffer)
	_, err = gz.Write(data)
	if err != nil {
		return []byte{}, err
	}

	err = gz.Close()
	if err != nil {
		return []byte{}, err
	}

	return buffer.Bytes(), nil
}

// DecompressObject - decompress gzip-compressed packet
func DecompressObject(data []byte, obj interface{}) error {
	var (
		reader       io.Reader
		resultBuffer bytes.Buffer
	)
	buffer := bytes.NewBuffer(data)

	reader, err := gzip.NewReader(buffer)
	if err != nil {
		return err
	}

	_, err = resultBuffer.ReadFrom(reader)
	if err != nil {
		return err
	}

	resultData := resultBuffer.Bytes()
	return json.Unmarshal(resultData, obj)
}
